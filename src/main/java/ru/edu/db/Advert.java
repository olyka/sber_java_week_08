package ru.edu.db;

import java.math.BigDecimal;
import java.util.Objects;

public class Advert {

    /**
     * Short text minimal length.
     */
    public static final int SHORT_TEXT_MINIMAL_LENGTH = 3;
    /**
     * Ad's id.
     */
    private Long id;

    /**
     * Ad's title.
     */
    private String title;

    /**
     * Ad's type.
     */
    private String type;

    /**
     * Ad's price.
     */
    private BigDecimal price;

    /**
     * Ad's text.
     */
    private String text;

    /**
     * Ad's author.
     */
    private String author;

    /**
     * Ad's e-mail.
     */
    private String email;

    /**
     * Ad's phone.
     */
    private String phone;

    /**
     * Ad's picture URL.
     */
    private String pictureUrl;

    /**
     * Get ad's id.
     * @return Ad's id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set ad's id.
     * @param adId Ad's id
     */
    public void setId(final Long adId) {
        this.id = adId;
    }

    /**
     * Get ad's title.
     * @return Ad's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set ad's title.
     * @param adTitle Ad's title
     */
    public void setTitle(final String adTitle) {
        this.title = adTitle;
    }

    /**
     * Get ad's type.
     * @return Ad's type
     */
    public String getType() {
        return type;
    }

    /**
     * Set ad's type.
     * @param adType Ad's type
     */
    public void setType(final String adType) {
        this.type = adType;
    }

    /**
     * Get ad's price.
     * @return Ad's price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Set ad's price.
     * @param adPrice Ad's price
     */
    public void setPrice(final BigDecimal adPrice) {
        this.price = adPrice;
    }

    /**
     * Get ad's text.
     * @return Ad's text
     */
    public String getText() {
        return text;
    }

    /**
     * Set ad's text.
     * @param adText Ad's text
     */
    public void setText(final String adText) {
        this.text = adText;
    }

    /**
     * Get ad's author.
     * @return Ad's author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set ad's author.
     * @param adAuthor Ad's author
     */
    public void setAuthor(final String adAuthor) {
        this.author = adAuthor;
    }

    /**
     * Get ad's e-mail.
     * @return Ad's e-mail
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set ad's e-mail.
     * @param adEmail Ad's e-mail
     */
    public void setEmail(final String adEmail) {
        this.email = adEmail;
    }

    /**
     * Get ad's phone.
     * @return Ad's phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set ad's phone.
     * @param adPhone Ad's phone
     */
    public void setPhone(final String adPhone) {
        this.phone = adPhone;
    }

    /**
     * Get ad's picture URL.
     * @return Ad's picture URL
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * Set ad's picture URI.
     * @param picUrl Ad's picture URL
     */
    public void setPictureUrl(final String picUrl) {
        this.pictureUrl = picUrl;
    }

    /**
     * Get ad's short text version.
     * @param length Length of the text to show
     * @return Short text
     */
    public String getShort(final int length) {

        int tempLength;
        if (length < SHORT_TEXT_MINIMAL_LENGTH) {
            tempLength = SHORT_TEXT_MINIMAL_LENGTH;
        } else {
            tempLength = length;
        }

        if (text.length() <= tempLength) {
            return text;
        }

        return text.substring(0, tempLength) + "...";

    }

    /**
     * Ad's string version.
     * @return Ad in string
     */
    @Override
    public String toString() {
        return "Advert{"
                + "id=" + id
                + ", title='" + title + '\''
                + ", type='" + type + '\''
                + ", price=" + price
                + ", text='" + text + '\''
                + ", author='" + author + '\''
                + ", email='" + email + '\''
                + ", phone='" + phone + '\''
                + ", pictureUrl='" + pictureUrl + '\''
                + '}';
    }

    /**
     * Ad's equals method.
     * @param o Ad object
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Advert)) {
            return false;
        }
        Advert advert = (Advert) o;
        return Objects.equals(getId(), advert.getId())
                && Objects.equals(getTitle(), advert.getTitle())
                && Objects.equals(getType(), advert.getType())
                && Objects.equals(getPrice(), advert.getPrice())
                && Objects.equals(getText(), advert.getText())
                && Objects.equals(getAuthor(), advert.getAuthor())
                && Objects.equals(getEmail(), advert.getEmail())
                && Objects.equals(getPhone(), advert.getPhone())
                && Objects.equals(getPictureUrl(), advert.getPictureUrl());
    }

    /**
     * Ad's hashcode method.
     * @return integer
     */
    @Override
    public int hashCode() {
        return Objects.hash(getId(),
                getTitle(),
                getType(),
                getPrice(),
                getText(),
                getAuthor(),
                getEmail(),
                getPhone(),
                getPictureUrl());
    }
}

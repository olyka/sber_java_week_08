package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class CRUD {

    /**
     * Select all query.
     */
    public static final String SELECT_ALL_SQL =
            "SELECT * FROM advertisments";

    /**
     * Select by id query.
     */
    public static final String SELECT_BY_ID =
            "SELECT * FROM advertisments WHERE id=?";

    /**
     * Insert ad query.
     */
    public static final String INSERT_SQL =
            "INSERT INTO advertisments "
            + "(title, type, text, price, author, email, phone, picture_url) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Update ad query.
     */
    public static final String UPDATE_SQL =
            "UPDATE advertisments "
            + "SET title=?, "
                    + "type=?, "
                    + "text=?, "
                    + "price=?, "
                    + "author=?, "
                    + "email=?,"
                    + " phone=?,"
                    + " picture_url=?"
            + "WHERE id=?";

    /**
     * CRUD instance.
     */
    private static CRUD instance;

    /**
     * DataSource object.
     */
    private final DataSource dataSource;

    /**
     * CRUD constructor.
     * @param source Datasource object
     */
    private CRUD(final DataSource source) {
        this.dataSource = source;
    }

    /**
     * Синглтон.
     *
     * @return CRUD
     */
    public static CRUD getInstance() {

        synchronized (CRUD.class) {
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env  =
                            (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource =
                            (DataSource) env.lookup("jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Set CRUD instance.
     * @param crud CRUD object
     */
    public static void setInstance(final CRUD crud) {
        CRUD.instance = crud;
    }

    /**
     * Get all ads from ad's table.
     * @return All ad's objects
     */
    public List<Advert> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    private List<Advert> query(final String sql,
                               final Object... values) {

        try (PreparedStatement statement =
                     getConnection().prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Advert> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Advert map(final ResultSet resultSet) throws SQLException {

        Advert advert = new Advert();

        advert.setId(resultSet.getLong("id"));
        advert.setTitle(resultSet.getString("title"));
        advert.setType(resultSet.getString("type"));
        advert.setPrice(new BigDecimal(resultSet.getString("price")));
        advert.setText(resultSet.getString("text"));
        advert.setAuthor(resultSet.getString("author"));
        advert.setEmail(resultSet.getString("email"));
        advert.setPhone(resultSet.getString("phone"));
        advert.setPictureUrl(resultSet.getString("picture_url"));

        return advert;

    }

    private Connection getConnection() {

        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    /**
     * Get ad's from database by ID.
     * @param id Ad's id.
     * @return Ad object
     */
    public Advert getById(final String id) {

        List<Advert> adverts = query(SELECT_BY_ID, id);

        if (adverts.isEmpty()) {
            return null;
        }
        return adverts.get(0);

    }

    /**
     * Insert or update advert record in database.
     * @param advert Advert object
     * @return 0 for inserting advert or 1 for updating advert
     */
    public int save(final Advert advert) {

        if (advert.getId() == null) {
            insert(advert);
            return 0;
        } else {
            update(advert);
            return 1;
        }

    }

    private int update(final Advert advert) {

        return execute(UPDATE_SQL,
                advert.getTitle(),
                advert.getType(),
                advert.getText(),
                advert.getPrice(),
                advert.getAuthor(),
                advert.getEmail(),
                advert.getPhone(),
                advert.getPictureUrl(),
                advert.getId());

    }

    private int insert(final Advert advert) {

        return execute(INSERT_SQL,
                advert.getTitle(),
                advert.getType(),
                advert.getText(),
                advert.getPrice().toPlainString(),
                advert.getAuthor(),
                advert.getEmail(),
                advert.getPhone(),
                advert.getPictureUrl());

    }

    private int execute(final String sql,
                        final Object... values) {

        try (PreparedStatement statement
                     = getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}

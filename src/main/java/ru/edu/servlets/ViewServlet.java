package ru.edu.servlets;

import ru.edu.db.CRUD;
import ru.edu.db.Advert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet {

    /**
     * CRUD instance.
     */
    private final CRUD crud = CRUD.getInstance();

    /**
     * Пример.
     *
     * @param req Request
     * @param resp Response
     * @throws ServletException Servlet Exception
     * @throws IOException I/O Exception
     */
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {

        String id = req.getParameter("id");

        if (id == null) {
            resp.sendRedirect("/index?error=id_param_missing");
            return;
        }

        Advert advert = crud.getById(id);

        if (advert == null) {
            resp.sendRedirect("/index?error=id_not_found");
            return;
        }

        req.setAttribute("advert", advert);

        req.getRequestDispatcher("/WEB-INF/view.jsp").forward(req, resp);
    }
}

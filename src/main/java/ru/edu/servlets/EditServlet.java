package ru.edu.servlets;

import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class EditServlet extends HttpServlet {

    /**
     * Подключаем синглтон.
     */
    private final CRUD crud = CRUD.getInstance();


    /**
     * Пример.
     *
     * @param req Request
     * @param resp Response
     * @throws ServletException Servlet Exception
     * @throws IOException I/O Exception
     */
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp
    ) throws ServletException, IOException {

        String id = req.getParameter("id");
        String status = req.getParameter("status");
        String error = req.getParameter("message");

        Advert advert = null;

        if (id != null) {
            advert = crud.getById(id);
            if ((status != null) && status.equals("ok")) {
                req.setAttribute("message", "Объявление успешно сохранено!");
            }
        } else if ((error != null) && error.equals("no_title")) {
            req.setAttribute("message",
                    "Ошибка! Заполните название объявления.");
        }

        req.setAttribute("advert", advert);

        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/WEB-INF/edit.jsp").forward(req, resp);
    }

    /**
     * Post request.
     * @param req Request
     * @param resp Response
     * @throws IOException I/O Exception
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws IOException {

        Map<String, String[]> form = req.getParameterMap();
        boolean formWithId = form.containsKey("id");
        String title = getStr(form, "title");

        Advert advert = new Advert();

        if (formWithId) {
            advert.setId(Long.parseLong(getStr(form, "id")));
        } else {
            if (title == null
                    || title.isEmpty()) {
                resp.sendRedirect("edit?message=no_title");
                return;
            }
        }

        advert.setTitle(title);
        advert.setType(getStr(form, "type"));
        advert.setText(getStr(form, "text"));
        advert.setPrice(new BigDecimal(getStr(form, "price")));
        advert.setAuthor(getStr(form, "author"));
        advert.setEmail(getStr(form, "email"));
        advert.setPhone(getStr(form, "phone"));
        advert.setPictureUrl(getStr(form, "picture_url"));

        crud.save(advert);

        if (!formWithId) {
            resp.sendRedirect("/index?status=ok");
        } else {
            resp.sendRedirect("/edit?status=ok&id=" + advert.getId());
        }

    }

    private String getStr(final Map<String, String[]> form,
                          final String id) {

        String[] param = form.get(id);
        if (param != null) {
            return param[0];
        } else {
            return null;
        }
    }


}

package ru.edu.servlets;

import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class IndexServlet extends HttpServlet {

    /**
     * Подключаем синглтон.
     */
    private final CRUD crud = CRUD.getInstance();


    /**
     * Пример.
     *
     * @param req Request
     * @param resp Response
     * @throws ServletException Servlet Exception
     * @throws IOException I/O Exception
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {

        String status = req.getParameter("status");
        if (status != null && status.equals("ok")) {
            req.setAttribute("message", "Объявление успешно сохранено!");
        }

        List<Advert> adverts = crud.getIndex();

        req.setAttribute("adverts", adverts);

        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }
}

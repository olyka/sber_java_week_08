<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html lang="ru">
<head>
  <title>${param.title}</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
  <link href="./css/style.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript">
    <!--
    function validate_form ( )
    {
      valid = true;
      if ( document.advert-form.title.value == "" )
      {
        alert ("Пожалуйста заполните поле 'Заголовок'.");
        valid = false;
      }
      return valid;
    }
    //-->
  </script>
</head>
<body>
<div class="page">
<jsp:include page="/WEB-INF/templates/header.jsp">
    <jsp:param name="title" value="Login page"/>
</jsp:include>


    <form method="post">
      <div class="content edit-form">
        <div class="form-item">
          <input id="login" name="login" placeholder="login"/>
        </div>
        <div class="form-item">
          <input id="password" name="password" placeholder="password" type="password"/>
        </div>
      </div>
      <div class="button-container">
          <button class="send-button">Войти</button>
      </div>
    </form>


<%@ include file="/WEB-INF/templates/footer.jsp"%>

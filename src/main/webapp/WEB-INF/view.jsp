<jsp:include page="/WEB-INF/templates/header.jsp">
  <jsp:param name="title" value="The advert"/>
</jsp:include>
<%@ page import="ru.edu.db.Advert" %>

  <div class="header">
    <h1>Доска объявлений</h1>
  </div>
  <%
 Advert rec = (Advert) request.getAttribute("advert");
  %>
    <div class="advert">

        <img src="<%=rec.getPictureUrl()%>" alt="<%=rec.getTitle()%>" title="<%=rec.getTitle()%>" />
        <h2><%=rec.getTitle()%></h2>
        <h3><%=rec.getType()%></h3>
        <div class="price">
          <%=rec.getPrice().toPlainString()%> руб.
        </div>

      <div class="adv-text">
        <h4>Текст объявления</h4>
        <p><%=rec.getText()%></p>
      </div>
      <div class="adv-contacts">
        <h4>Контактные данные</h4>
        <p>Автор: <span><%=rec.getAuthor()%></span></p>
        <p>Email: <span><%=rec.getEmail()%></span></p>
        <p>Телефон: <span><%=rec.getPhone()%></span></p>
      </div>
    </div>

<div class="button-container">
    <a href="edit?id=<%=rec.getId()%>">
        <span class="send-button">Редактировать объявление</span>
    </a>
</div>

<%@ include file="/WEB-INF/templates/footer.jsp"%>
<jsp:include page="/WEB-INF/templates/header.jsp">
    <jsp:param name="title" value="Bulletin board" />
</jsp:include>
<%@ page import="java.util.List" %>
<%@ page import="ru.edu.db.Advert" %>

    <div class="header">
        <h1>Доска объявлений</h1>
    </div>

<%
    String message = (String) request.getAttribute("message");
    if ((message != null) && (message.length() > 0)) { %>
<div class="message">
    <p><%= message %></p>
</div>
<% } %>

        <%
        List<Advert> adverts = (List<Advert>) request.getAttribute("adverts");

        if (adverts.isEmpty()) { %>
          <h2>Записей нет</h2>
        <% } else {
          for (Advert rec : adverts) {
        %>
          <div class="advert">
            <div class="img">
                <img src="<%= rec.getPictureUrl() %>?t=<%= java.time.Instant.now().getNano() + rec.getId() %>" alt="<%= rec.getTitle() %>" title="<%= rec.getTitle() %>" /></div>
            <div class="content">
                <h3><%= rec.getTitle() %></h3>
                <div class="type"><%= rec.getType() %></div>
                <div class="short-description"><%= rec.getShort(300) %></div>
                <div class="price"><%= rec.getPrice().toPlainString() %></div>
                <div class="links">
                    <a href="view?id=<%= rec.getId() %>">Просмотр</a> | <a href="edit?id=<%= rec.getId() %>">Редактировать</a>
                </div>
            </div>
          </div>
        <% }
        } %>

    <div class="button-container">
      <a href="edit">
        <span class="send-button">Разместить объявление</span>
      </a>
    </div>

<%@ include file="/WEB-INF/templates/footer.jsp" %>

<jsp:include page="/WEB-INF/templates/header.jsp">
    <jsp:param name="title" value="Editing your ad" />
</jsp:include>
<%@ page import="ru.edu.db.Advert" %>
<%
  Advert advert = (Advert) request.getAttribute("advert");
  String message = (String) request.getAttribute("message");
  boolean isEdit = advert != null;
%>
    <div class="header">
        <h1>Доска объявлений</h1>
        <% if (isEdit) { %>
        <h2>Редактировать объявление</h2>
        <% } else { %>
        <h2>Разместить объявление</h2>
        <% } %>
    </div>
    <% if ((message != null) && (message.length() > 0)) { %>
    <div class="message">
      <p><%= message %></p>
    </div>
    <% } %>
    <form name="advert-form" method="post" onsubmit="return validate_form();">
      <div class="edit-form">
        <div class="form-item">
            <h4><label for="title">Заголовок:</label></h4>
          <input type="text" id="title" name="title" placeholder="Заголовок" value="<%=(isEdit ? advert.getTitle(): "")%>"/>
        </div>
        <div class="form-item">
            <h4><label for="type">Тип услуги:</label></h4>
          <select id="type" name="type" placeholder="Тип" >
            <option value="Услуги" <%=isEdit && "Услуги".equals(advert.getType()) ? "selected": ""%>>Услуги</option>
            <option value="Покупка" <%=isEdit && "Покупка".equals(advert.getType()) ? "selected": ""%>>Покупка</option>
            <option value="Продажа" <%=isEdit && "Продажа".equals(advert.getType()) ? "selected": ""%>>Продажа</option>
          </select>
        </div>
        <div class="form-item">
            <h4><label for="text">Текст объявления:</label></h4>
          <textarea id="text" name="text" placeholder="Текст объявления"><%=(isEdit ? advert.getText(): "")%></textarea>
        </div>
        <div class="form-item">
            <h4><label for="price">Цена:</label></h4>
          <input type="text" id="price" name="price" placeholder="Цена" value="<%= (isEdit ? advert.getPrice().toPlainString() : "0")%>"/>
        </div>
        <div class="form-item">
            <h4><label for="author">Автор:</label></h4>
          <input type="text" id="author" name="author" placeholder="Автор" value="<%=(isEdit ? advert.getAuthor(): "")%>"/>
        </div>
        <div class="form-item">
            <h4><label for="email">E-mail:</label></h4>
          <input type="email" id="email" name="email" placeholder="Email" value="<%=(isEdit ? advert.getEmail(): "")%>"/>
        </div>
        <div class="form-item">
            <h4><label for="phone">Телефон:</label></h4>
          <input type="text" id="phone" name="phone" placeholder="Телефон" value="<%=(isEdit ? advert.getPhone(): "")%>"/>
        </div>
        <div class="form-item">
          <h4><label for="picture_url">Изображение:</label></h4>
          <input type="text" id="picture_url" name="picture_url" placeholder="Изображение" value="<%=(isEdit ? advert.getPictureUrl(): "")%>"/>
        </div>
      </div>
      <div class="button-container">
        <% if (isEdit) { %>
          <button class="send-button">Сохранить изменения</button>
        <% } else { %>
          <button class="send-button">Разместить объявление</button>
        <% } %>
      </div>
    </form>



<%@ include file="/WEB-INF/templates/footer.jsp" %>

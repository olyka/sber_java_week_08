package ru.edu.db;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static ru.edu.db.CRUDTest.ads;

public class AdvertTest {

    @Test
    public void testEquals_Symmetric() {

        Advert x = new Advert();
        x.setId(Long.valueOf(ads[0][0]));
        x.setTitle(ads[0][1]);
        x.setType(ads[0][2]);
        x.setText(ads[0][3]);
        x.setPrice(new BigDecimal(ads[0][4]));
        x.setAuthor(ads[0][5]);
        x.setEmail(ads[0][6]);
        x.setPhone(ads[0][7]);
        x.setPictureUrl(ads[0][8]);

        Advert y = new Advert();
        y.setId(Long.valueOf(ads[0][0]));
        y.setTitle(ads[0][1]);
        y.setType(ads[0][2]);
        y.setText(ads[0][3]);
        y.setPrice(new BigDecimal(ads[0][4]));
        y.setAuthor(ads[0][5]);
        y.setEmail(ads[0][6]);
        y.setPhone(ads[0][7]);
        y.setPictureUrl(ads[0][8]);

        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());

        assertTrue(x.equals(x));

        assertFalse(x.equals(new Object()));

        assertFalse(ads[0].equals(ads[1]));
        assertFalse(ads[1].equals(ads[0]));

    }

    @Test
    public void getShortTest() {

        Advert ad = new Advert();
        ad.setText("Some text");

        assertEquals("Som...", ad.getShort(2));
        assertEquals("Some...", ad.getShort(4));
        assertEquals("Some text", ad.getShort(100));


    }


}

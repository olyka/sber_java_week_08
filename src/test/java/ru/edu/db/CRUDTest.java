package ru.edu.db;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Spy;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class CRUDTest {

    static final String[][] ads = {
            {"1",
                    "title1",
                    "type1",
                    "text1",
                    "10",
                    "author1",
                    "email1",
                    "phone1",
                    "picture_url1"
            },
            {"2",
                    "title2",
                    "type2",
                    "text2",
                    "20",
                    "author2",
                    "email2",
                    "phone2",
                    "picture_url2"
            },
    };

    public final static DataSource dataSourceMock = mock(DataSource.class);

    private final static Connection connectionMock = mock(Connection.class);

    private final static PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    private final static PreparedStatement mockPreparedStatementById = mock(PreparedStatement.class);

    @Spy
    private PreparedStatement mockPreparedStatementUpdateInsert = mock(PreparedStatement.class);

    private final static ResultSet mockResultSet = mock(ResultSet.class);
    private final static ResultSet mockResultSetById = mock(ResultSet.class);

    @BeforeClass
    public static void setupJndi() throws NamingException {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.rebind("java:/comp/env", ic);
        ic.rebind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.rebind("jdbc/dbLink", dataSourceMock);

    }


    @Before
    public void testJndi() throws NamingException, SQLException {

        Context ctx = new InitialContext();
        Context env  = (Context)ctx.lookup("java:/comp/env");
        DataSource dataSource = (DataSource) env.lookup("jdbc/dbLink");
        assertEquals(dataSourceMock, dataSource);
        when(dataSource.getConnection()).thenReturn(connectionMock);

    }

    public void initByIdMocks() throws SQLException {

        when(dataSourceMock.getConnection().
                prepareStatement(CRUD.SELECT_BY_ID)).
                thenReturn(mockPreparedStatementById);
        when(mockPreparedStatementById.executeQuery()).thenReturn(mockResultSetById);
        when(mockResultSetById.next()).thenReturn(true).thenReturn(false);
        when(mockResultSetById.getLong("id")).thenReturn(Long.valueOf(ads[0][0]));
        when(mockResultSetById.getString("title")).thenReturn(ads[0][1]);
        when(mockResultSetById.getString("type")).thenReturn(ads[0][2]);
        when(mockResultSetById.getString("text")).thenReturn(ads[0][3]);
        when(mockResultSetById.getString("price")).thenReturn(ads[0][4]);
        when(mockResultSetById.getString("author")).thenReturn(ads[0][5]);
        when(mockResultSetById.getString("email")).thenReturn(ads[0][6]);
        when(mockResultSetById.getString("phone")).thenReturn(ads[0][7]);
        when(mockResultSetById.getString("picture_url")).thenReturn(ads[0][8]);

    }

    public static void initIndexMocks() throws SQLException {

        when(dataSourceMock.getConnection().
                prepareStatement(CRUD.SELECT_ALL_SQL)).
                thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(mockResultSet.getLong("id")).thenReturn(Long.valueOf(ads[0][0])).thenReturn(Long.valueOf(ads[1][0]));
        when(mockResultSet.getString("title")).thenReturn(ads[0][1]).thenReturn(ads[1][1]);
        when(mockResultSet.getString("type")).thenReturn(ads[0][2]).thenReturn(ads[1][2]);
        when(mockResultSet.getString("text")).thenReturn(ads[0][3]).thenReturn(ads[1][3]);
        when(mockResultSet.getString("price")).thenReturn(ads[0][4]).thenReturn(ads[1][4]);
        when(mockResultSet.getString("author")).thenReturn(ads[0][5]).thenReturn(ads[1][5]);
        when(mockResultSet.getString("email")).thenReturn(ads[0][6]).thenReturn(ads[1][6]);
        when(mockResultSet.getString("phone")).thenReturn(ads[0][7]).thenReturn(ads[1][7]);
        when(mockResultSet.getString("picture_url")).thenReturn(ads[0][7]).thenReturn(ads[1][8]);

    }

    public void getByNotExistedIdTest() throws SQLException {

        CRUD crud = CRUD.getInstance();
        assertNotNull(crud);

        when(dataSourceMock.getConnection().
                prepareStatement(CRUD.SELECT_BY_ID)).
                thenReturn(mockPreparedStatementById);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(false);

        Advert ad = crud.getById("1");
        assertEquals(null, ad);

    }

    @Test
    public void getByIdTest() throws SQLException {

        CRUD crud = CRUD.getInstance();
        assertNotNull(crud);

        initByIdMocks();

        Advert ad = crud.getById(ads[0][0]);
        System.out.println(ad);

        assertEquals(Long.valueOf(ads[0][0]), ad.getId());
        assertEquals(ads[0][1], ad.getTitle());
        assertEquals(ads[0][2], ad.getType());
        assertEquals(ads[0][3], ad.getText());
        assertEquals(new BigDecimal(ads[0][4]), ad.getPrice());
        assertEquals(ads[0][5], ad.getAuthor());
        assertEquals(ads[0][6], ad.getEmail());
        assertEquals(ads[0][7], ad.getPhone());
        assertEquals(ads[0][8], ad.getPictureUrl());

    }

    @Test
    public void getIndexTest() throws SQLException {

        CRUD crud = CRUD.getInstance();
        assertNotNull(crud);

        initIndexMocks();

        List<Advert> list = crud.getIndex();

        Advert ad = new Advert();
        ad.setId(Long.valueOf(ads[1][0]));
        ad.setTitle(ads[1][1]);
        ad.setType(ads[1][2]);
        ad.setText(ads[1][3]);
        ad.setPrice(new BigDecimal(ads[1][4]));
        ad.setAuthor(ads[1][5]);
        ad.setEmail(ads[1][6]);
        ad.setPhone(ads[1][7]);
        ad.setPictureUrl(ads[1][8]);

        assertNotNull(list);
        assertEquals(list.get(1), ad);

    }

    @Test
    public void insertTest() throws SQLException {

        CRUD crud = CRUD.getInstance();
        assertNotNull(crud);

        when(dataSourceMock.getConnection().prepareStatement(CRUD.INSERT_SQL))
                .thenReturn(mockPreparedStatementUpdateInsert);

        Advert ad = new Advert();
        ad.setTitle(ads[1][1]);
        ad.setType(ads[1][2]);
        ad.setText(ads[1][3]);
        ad.setPrice(new BigDecimal(ads[1][4]));
        ad.setAuthor(ads[1][5]);
        ad.setEmail(ads[1][6]);
        ad.setPhone(ads[1][7]);
        ad.setPictureUrl(ads[1][8]);

        int value = crud.save(ad);
        assertEquals(0, value);

        for (int i = 1; i < 8; i++) {
            verify(mockPreparedStatementUpdateInsert, times(1)).setObject(i, ads[1][i]);
        }

    }

    @Test
    public void updateTest() throws SQLException {

        CRUD crud = CRUD.getInstance();
        assertNotNull(crud);

        when(dataSourceMock.getConnection().prepareStatement(CRUD.UPDATE_SQL))
                .thenReturn(mockPreparedStatementUpdateInsert);

        Advert ad = new Advert();
        ad.setId(Long.valueOf(ads[1][0]));
        ad.setTitle(ads[1][1]);
        ad.setType(ads[1][2]);
        ad.setText(ads[1][3]);
        ad.setPrice(new BigDecimal(ads[1][4]));
        ad.setAuthor(ads[1][5]);
        ad.setEmail(ads[1][6]);
        ad.setPhone(ads[1][7]);
        ad.setPictureUrl(ads[1][8]);

        int value = crud.save(ad);
        assertEquals(1, value);

        for (int i = 1; i < 8; i++) {
            if (i != 4) {
                verify(mockPreparedStatementUpdateInsert, times(1)).setObject(i,
                        ads[1][i]);
            }
        }
        verify(mockPreparedStatementUpdateInsert, times(1)).setObject(4, new BigDecimal(ads[1][4]));
        verify(mockPreparedStatementUpdateInsert, times(1)).setObject(9, Long.valueOf(ads[1][0]));

    }

}
package ru.edu.servlets;

import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class IndexServletTest {

    public static final String PATH = "/WEB-INF/index.jsp";

    @Test
    public void doGet() throws ServletException, IOException {

        CRUD crudMock = mock(CRUD.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        HttpServletRequest req = mock(HttpServletRequest.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        CRUD.setInstance(crudMock);

        IndexServlet servlet = new IndexServlet();
        List<Advert> ads = Collections.emptyList();

        when(crudMock.getIndex()).thenReturn(ads);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(req, resp);

        verify(req, times(1)).getRequestDispatcher(PATH);
        verify(req, times(1)).setAttribute("adverts", ads);
        verify(dispatcher).forward(req, resp);

    }
}
package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class ViewServletTest {

    public static final String PATH = "/WEB-INF/view.jsp";
    public static final String ERROR_ID_PARAM_MISSING = "/index?error=id_param_missing";
    public static final String ERROR_ID_NOT_FOUND = "/index?error=id_not_found";

    final CRUD crudMock = mock(CRUD.class);
    final HttpServletResponse resp = mock(HttpServletResponse.class);
    final HttpServletRequest req = mock(HttpServletRequest.class);
    final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    @Before
    public void setInstance() {
        CRUD.setInstance(crudMock);
    }

    @Test
    public void doGet() throws ServletException, IOException {

        ViewServlet servlet = new ViewServlet();
        Advert ad = new Advert();

        when(crudMock.getById(anyString())).thenReturn(ad);
        when(req.getParameter("id")).thenReturn(anyString());
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doGet(req, resp);

        verify(req, times(1)).getRequestDispatcher(PATH);
        verify(req, times(1)).setAttribute("advert", ad);
        verify(dispatcher).forward(req, resp);

    }

    @Test
    public void doGetWithNoId() throws ServletException, IOException {

        ViewServlet servlet = new ViewServlet();
        when(req.getParameter("id")).thenReturn(null);

        servlet.doGet(req, resp);

        verify(resp, times(1)).sendRedirect(ERROR_ID_PARAM_MISSING);
        verify(resp, times(0)).sendRedirect(ERROR_ID_NOT_FOUND);

    }

    @Test
    public void doGetIdNotFound() throws ServletException, IOException {

        ViewServlet servlet = new ViewServlet();

        when(crudMock.getById(anyString())).thenReturn(null);
        when(req.getParameter("id")).thenReturn(anyString());

        servlet.doGet(req, resp);

        verify(resp, times(0)).sendRedirect(ERROR_ID_PARAM_MISSING);
        verify(resp, times(1)).sendRedirect(ERROR_ID_NOT_FOUND);

    }


}
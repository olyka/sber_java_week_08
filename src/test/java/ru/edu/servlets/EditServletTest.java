package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;

import static org.mockito.Mockito.*;

public class EditServletTest {

    public static final String PATH = "/WEB-INF/edit.jsp";
    public static final String INDEX_STATUS_OK = "/index?status=ok";
    public static final String EDIT_STATUS_OK_ID = "/edit?status=ok&id=";
    public static final String EDIT_MESSAGE_NO_TITLE = "edit?message=no_title";
    public static final String MESSAGE_SAVE_OK = "Объявление успешно сохранено!";
    public static final String STATUS_OK = "ok";
    public static final String ERROR_NO_TITLE = "no_title";
    public static final String MESSAGE_ERROR_NO_TITLE = "Ошибка! Заполните название объявления.";

    final CRUD crudMock = mock(CRUD.class);
    final HttpServletResponse resp = mock(HttpServletResponse.class);
    final HttpServletRequest req = mock(HttpServletRequest.class);
    final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    @Before
    public void setInstance() {
        CRUD.setInstance(crudMock);
    }

    @Test
    public void doGet() throws ServletException, IOException {

        EditServlet servlet = new EditServlet();

        String id = "1";
        Advert ad = new Advert();
        ad.setId(Long.valueOf(id));

        when(crudMock.getById(id)).thenReturn(ad);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(req.getParameter("id")).thenReturn(id);

        servlet.doGet(req, resp);

        verify(req, times(1)).getRequestDispatcher(PATH);
        verify(req, times(1)).setAttribute("advert", ad);
        verify(resp, times(1)).setCharacterEncoding("UTF-8");
        verify(dispatcher).forward(req, resp);

    }

    @Test
    public void doGetSaveOk() throws ServletException, IOException {

        EditServlet servlet = new EditServlet();

        String id = "1";
        Advert ad = new Advert();
        ad.setId(Long.valueOf(id));

        when(crudMock.getById(id)).thenReturn(ad);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(req.getParameter("id")).thenReturn(id);
        when(req.getParameter("status")).thenReturn(STATUS_OK);

        servlet.doGet(req, resp);

        verify(req, times(1)).getRequestDispatcher(PATH);
        verify(req, times(1)).setAttribute("message", MESSAGE_SAVE_OK);
        verify(resp, times(1)).setCharacterEncoding("UTF-8");
        verify(dispatcher).forward(req, resp);

    }

    @Test
    public void doGetWithError() throws ServletException, IOException {

        EditServlet servlet = new EditServlet();

        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(req.getParameter("id")).thenReturn(null);
        when(req.getParameter("message")).thenReturn(ERROR_NO_TITLE);

        servlet.doGet(req, resp);

        verify(req, times(1)).getRequestDispatcher(PATH);
        verify(req, times(1)).setAttribute("message", MESSAGE_ERROR_NO_TITLE);
        verify(resp, times(1)).setCharacterEncoding("UTF-8");
        verify(dispatcher).forward(req, resp);

    }



    @Test
    public void doPost() throws IOException {

        EditServlet servlet = new EditServlet();

        HashMap<String, String[]> paramMap = spy(new HashMap<>());
        String id = String.valueOf(123L);

        paramMap.put("id", new String[]{id});
        paramMap.put("title", new String[]{"title"});
        paramMap.put("type", new String[]{"type"});
        paramMap.put("price", new String[]{"10"});
        paramMap.put("text", new String[]{"text"});
        paramMap.put("author", new String[]{"author"});
        paramMap.put("email", new String[]{"email"});
        paramMap.put("phone", new String[]{"phone"});
        paramMap.put("picture_url", new String[]{"picture_url"});

        when(req.getParameterMap()).thenReturn(paramMap);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doPost(req, resp);

        verify(paramMap, times(1)).get("id");
        verify(paramMap, times(1)).get("title");
        verify(paramMap, times(1)).get("type");
        verify(paramMap, times(1)).get("price");
        verify(paramMap, times(1)).get("text");
        verify(paramMap, times(1)).get("author");
        verify(paramMap, times(1)).get("email");
        verify(paramMap, times(1)).get("phone");
        verify(paramMap, times(1)).get("picture_url");

        verify(crudMock, times(1)).save(any(Advert.class));

        verify(resp, times(1)).sendRedirect(EDIT_STATUS_OK_ID +  id);

    }



    @Test
    public void doPostWithNoId() throws IOException {

        EditServlet servlet = new EditServlet();

        HashMap<String, String[]> paramMap = spy(new HashMap<>());

        paramMap.put("title", new String[]{"title"});
        paramMap.put("price", new String[]{"10"});

        when(req.getParameterMap()).thenReturn(paramMap);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doPost(req, resp);

        verify(paramMap, times(0)).get("id");
        verify(paramMap, times(1)).get("title");
        verify(paramMap, times(1)).get("type");
        verify(paramMap, times(1)).get("price");
        verify(paramMap, times(1)).get("text");
        verify(paramMap, times(1)).get("author");
        verify(paramMap, times(1)).get("email");
        verify(paramMap, times(1)).get("phone");
        verify(paramMap, times(1)).get("picture_url");

        verify(crudMock, times(1)).save(any(Advert.class));

        verify(resp, times(1)).sendRedirect(INDEX_STATUS_OK);

    }


    @Test
    public void doPostWithNoTitle() throws IOException {

        EditServlet servlet = new EditServlet();

        HashMap<String, String[]> paramMap = spy(new HashMap<>());
        paramMap.put("title", new String[]{""});

        when(req.getParameterMap()).thenReturn(paramMap);
        when(req.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        servlet.doPost(req, resp);

        verify(paramMap, times(0)).get("id");
        verify(paramMap, times(1)).get("title");
        verify(paramMap, times(0)).get("type");
        verify(paramMap, times(0)).get("price");
        verify(paramMap, times(0)).get("text");
        verify(paramMap, times(0)).get("author");
        verify(paramMap, times(0)).get("email");
        verify(paramMap, times(0)).get("phone");
        verify(paramMap, times(0)).get("picture_url");

        verify(crudMock, times(0)).save(any(Advert.class));

        verify(resp, times(1)).sendRedirect(EDIT_MESSAGE_NO_TITLE);

    }


}